import execCmd from 'exec-sh'
import colors from 'colors'
import cliHeader from '@desco/cli-header'
import path from 'path'
import prompts from 'prompts'
import cliSpinners from 'cli-spinners'
import logUpdate from 'log-update-async-hook'
import fs from 'fs'

import { IProgressStep, } from '../types'
import { options, } from '../argv'

export { options, } from '../argv'

export const crioDir: string = path.dirname(__dirname)
export const baseDir: string = path.resolve()
export const projectName = ''

export function start () {
  cliHeader({
    title: 'Crio v0.0.0',
  })

  if (options.dev) {
    console.log(colors.black.bgWhite(' CRIO running in developer mode \n'))
  }

  menu()
}

export async function menu (): Promise<void> {
  const { option, } = await prompts({
    type: 'select',
    name: 'option',
    message: 'What do you want to do?',
    choices: [
      {
        title: 'Create new project',
        description: 'Create a new CRIO project',
        value: require('../project').create,
      },
      {
        title: 'Install a new CRIO module',
        description: 'Allows you to install a new module to be used by the CRIO CLI',
        value: require('../modules').install,
        disabled: false,
      },
    ],
  })

  option()
}

export async function progress (messages: (IProgressStep | undefined)[], finalMessage?: string): Promise<void> {
  const frames = cliSpinners.aesthetic.frames

  let i = 0

  let message = ''

  const id = setInterval((): void => {
    const frame = frames[i = ++i % frames.length]
   
    logUpdate(`${frame} ${message}`)
  }, cliSpinners.aesthetic.interval)

  async function processMessages (messages: IProgressStep[]): Promise<void> {
    const messageObject: IProgressStep = messages.shift() as IProgressStep
    message = ' ' + messageObject.message

    await messageObject.handler()
      .catch(e => {
        error(`\n${e}\n`)
        process.exit()
      })

    if (messages.length > 0) {
      return processMessages(messages)
    }
  }

  return processMessages(messages.filter(i => i) as IProgressStep[]).then(() => {
    clearInterval(id)

    logUpdate(`${frames[6]}  ${finalMessage || 'Done!'}`)

    logUpdate.done()

    return Promise.resolve()
  })
}

export function subTitle (title: string): void {
  console.log(`\n${colors.bgBlack.white.bold(` ${title} `)}\n`)
}

export async function createDir (_names: string | string[], ignoreExisting?: boolean): Promise<boolean> {
  if (typeof _names === 'string') return createDir([ _names as string, ])

  const names: string[] = [ ..._names as string[], ]
  const name: string = names.shift() as string

  if (!await fs.existsSync(name)){
    await fs.mkdirSync(name)

    return names.length > 0 ? createDir(names, ignoreExisting) : true
  }
  else if (!ignoreExisting) {
    return true
  }
  else {
    error(`A directory named "${name}" already exists`)

    return false
  }
}

export function error (msg: string): void {
  console.log(colors.red(msg))
}

export function exec (cmd: string, cwd: string): Promise<any> {
  return execCmd.promise(cmd, { detached: true, cwd, })
}

export function objectToString (json: object) {
  return JSON.stringify(json, null, 2)
}

start()