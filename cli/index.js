"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.objectToString = exports.exec = exports.error = exports.createDir = exports.subTitle = exports.progress = exports.menu = exports.start = exports.projectName = exports.baseDir = exports.crioDir = exports.options = void 0;
var exec_sh_1 = __importDefault(require("exec-sh"));
var colors_1 = __importDefault(require("colors"));
var cli_header_1 = __importDefault(require("@desco/cli-header"));
var path_1 = __importDefault(require("path"));
var prompts_1 = __importDefault(require("prompts"));
var cli_spinners_1 = __importDefault(require("cli-spinners"));
var log_update_async_hook_1 = __importDefault(require("log-update-async-hook"));
var fs_1 = __importDefault(require("fs"));
var argv_1 = require("../argv");
var argv_2 = require("../argv");
Object.defineProperty(exports, "options", { enumerable: true, get: function () { return argv_2.options; } });
exports.crioDir = path_1.default.dirname(__dirname);
exports.baseDir = path_1.default.resolve();
exports.projectName = '';
function start() {
    (0, cli_header_1.default)({
        title: 'Crio v0.0.0',
    });
    if (argv_1.options.dev) {
        console.log(colors_1.default.black.bgWhite(' CRIO running in developer mode \n'));
    }
    menu();
}
exports.start = start;
function menu() {
    return __awaiter(this, void 0, void 0, function () {
        var option;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, (0, prompts_1.default)({
                        type: 'select',
                        name: 'option',
                        message: 'What do you want to do?',
                        choices: [
                            {
                                title: 'Create new project',
                                description: 'Create a new CRIO project',
                                value: require('../project').create,
                            },
                            {
                                title: 'Install a new CRIO module',
                                description: 'Allows you to install a new module to be used by the CRIO CLI',
                                value: require('../modules').install,
                                disabled: false,
                            },
                        ],
                    })];
                case 1:
                    option = (_a.sent()).option;
                    option();
                    return [2 /*return*/];
            }
        });
    });
}
exports.menu = menu;
function progress(messages, finalMessage) {
    return __awaiter(this, void 0, void 0, function () {
        function processMessages(messages) {
            return __awaiter(this, void 0, void 0, function () {
                var messageObject;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            messageObject = messages.shift();
                            message = ' ' + messageObject.message;
                            return [4 /*yield*/, messageObject.handler()
                                    .catch(function (e) {
                                    error("\n".concat(e, "\n"));
                                    process.exit();
                                })];
                        case 1:
                            _a.sent();
                            if (messages.length > 0) {
                                return [2 /*return*/, processMessages(messages)];
                            }
                            return [2 /*return*/];
                    }
                });
            });
        }
        var frames, i, message, id;
        return __generator(this, function (_a) {
            frames = cli_spinners_1.default.aesthetic.frames;
            i = 0;
            message = '';
            id = setInterval(function () {
                var frame = frames[i = ++i % frames.length];
                (0, log_update_async_hook_1.default)("".concat(frame, " ").concat(message));
            }, cli_spinners_1.default.aesthetic.interval);
            return [2 /*return*/, processMessages(messages.filter(function (i) { return i; })).then(function () {
                    clearInterval(id);
                    (0, log_update_async_hook_1.default)("".concat(frames[6], "  ").concat(finalMessage || 'Done!'));
                    log_update_async_hook_1.default.done();
                    return Promise.resolve();
                })];
        });
    });
}
exports.progress = progress;
function subTitle(title) {
    console.log("\n".concat(colors_1.default.bgBlack.white.bold(" ".concat(title, " ")), "\n"));
}
exports.subTitle = subTitle;
function createDir(_names, ignoreExisting) {
    return __awaiter(this, void 0, void 0, function () {
        var names, name;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (typeof _names === 'string')
                        return [2 /*return*/, createDir([_names,])];
                    names = __spreadArray([], _names, true);
                    name = names.shift();
                    return [4 /*yield*/, fs_1.default.existsSync(name)];
                case 1:
                    if (!!(_a.sent())) return [3 /*break*/, 3];
                    return [4 /*yield*/, fs_1.default.mkdirSync(name)];
                case 2:
                    _a.sent();
                    return [2 /*return*/, names.length > 0 ? createDir(names, ignoreExisting) : true];
                case 3:
                    if (!ignoreExisting) {
                        return [2 /*return*/, true];
                    }
                    else {
                        error("A directory named \"".concat(name, "\" already exists"));
                        return [2 /*return*/, false];
                    }
                    _a.label = 4;
                case 4: return [2 /*return*/];
            }
        });
    });
}
exports.createDir = createDir;
function error(msg) {
    console.log(colors_1.default.red(msg));
}
exports.error = error;
function exec(cmd, cwd) {
    return exec_sh_1.default.promise(cmd, { detached: true, cwd: cwd, });
}
exports.exec = exec;
function objectToString(json) {
    return JSON.stringify(json, null, 2);
}
exports.objectToString = objectToString;
start();
//# sourceMappingURL=index.js.map