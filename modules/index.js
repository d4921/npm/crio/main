"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.resumeModules = exports.install = exports.modules = void 0;
var colors_1 = __importDefault(require("colors"));
var prompts_1 = __importDefault(require("prompts"));
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
var cli_1 = require("../cli");
var list_1 = require("./list");
var list_2 = require("./list");
Object.defineProperty(exports, "modules", { enumerable: true, get: function () { return list_2.modules; } });
function install() {
    return __awaiter(this, void 0, void 0, function () {
        var oficialModules, otherModules, installModules, install, update;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, (0, prompts_1.default)({
                        type: 'multiselect',
                        name: 'oficialModules',
                        message: "Which of the following ".concat(colors_1.default.cyan('official'), " modules do you want to install?"),
                        choices: [
                            { title: '@crio/express', value: '@crio/express', disabled: false, },
                            { title: '@crio/typeorm', value: '@crio/typeorm', disabled: true, },
                        ],
                    })];
                case 1:
                    oficialModules = (_a.sent()).oficialModules;
                    return [4 /*yield*/, (0, prompts_1.default)({
                            type: 'list',
                            name: 'otherModules',
                            message: "In addition to the modules selected earlier, which others do you want to install? (Separate by comma)",
                        })];
                case 2:
                    otherModules = (_a.sent()).otherModules;
                    installModules = __spreadArray(__spreadArray([], oficialModules, true), otherModules, true);
                    install = {
                        message: 'Installing...',
                        handler: function () { return __awaiter(_this, void 0, void 0, function () {
                            var cmd;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        cmd = cli_1.options.dev ? 'link' : 'install -g';
                                        return [4 /*yield*/, (0, cli_1.exec)("npm ".concat(cmd, " ").concat(installModules.join(' ')), cli_1.crioDir)
                                                .catch(function () { return Promise.reject('There was an error installing the package. Is the package name correct?'); })];
                                    case 1:
                                        _a.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); },
                    };
                    update = {
                        message: 'Updating module list...',
                        handler: function () { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, fs_1.default.writeFileSync(path_1.default.join(cli_1.crioDir, 'modules', 'list.ts'), 'export const modules: string[] = ' + JSON.stringify(installModules).split('"').join('\''))];
                                    case 1:
                                        _a.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); },
                    };
                    (0, cli_1.progress)([install, update,]);
                    return [2 /*return*/];
            }
        });
    });
}
exports.install = install;
function resumeModules() {
    return list_1.modules.map(function (i) {
        var module = require(i);
        return {
            title: module.title,
            description: module.description,
            value: module.value,
        };
    });
}
exports.resumeModules = resumeModules;
//# sourceMappingURL=index.js.map