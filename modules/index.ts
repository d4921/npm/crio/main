import colors from 'colors'

import prompts from 'prompts'
import fs from 'fs'
import path from 'path'

import { progress, crioDir, options, exec, } from '../cli'
import { IProgressStep, IModuleCli, } from '../types'

import { modules, } from './list'

export { modules, } from './list'

export async function install (): Promise<void> {
  const { oficialModules, } = await prompts({
    type: 'multiselect',
    name: 'oficialModules',
    message: `Which of the following ${colors.cyan('official')} modules do you want to install?`,
    choices: [
      { title: '@crio/express', value: '@crio/express', disabled: false, },
      { title: '@crio/typeorm', value: '@crio/typeorm', disabled: true, },
    ],
  })

  const { otherModules, } = await prompts({
    type: 'list',
    name: 'otherModules',
    message: `In addition to the modules selected earlier, which others do you want to install? (Separate by comma)`,
  })

  const installModules: string[] = [ ...oficialModules, ...otherModules, ]

  const install: IProgressStep = {
    message:'Installing...',
    handler: async () => {
      const cmd = options.dev ? 'link' : 'install -g'

      await exec(`npm ${cmd} ${installModules.join(' ')}`, crioDir)
        .catch(() => Promise.reject('There was an error installing the package. Is the package name correct?'))
    },
  }

  const update: IProgressStep = {
    message:'Updating module list...',
    handler: async () => {
      await fs.writeFileSync(
        path.join(crioDir,  'modules', 'list.ts'),
        'export const modules: string[] = ' + JSON.stringify(installModules).split('"').join('\'')
      )
    },
  }

  progress([ install, update, ])
}

export function resumeModules (): IModuleCli[] {
  return modules.map((i) => {
    const module = require(i)

    return {
      title: module.title,
      description: module.description,
      value: module.value,
    } as IModuleCli
  })
}