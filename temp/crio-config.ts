export const modules = [
  "@crio/express"
]
export const EXPRESS_PORT = 3000
export const EXPRESS_BODY = true
export const EXPRESS_CORS = true
export const EXPRESS_PARAMETER_LIMIT = 1000
export const EXPRESS_REQUEST_LIMIT = '100kb'
export const EXPRESS_SERVER_QUERYSTRING = true
export const EXPRESS_STATIC_FOLDERS = [
  "a"
]