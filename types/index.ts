export { IArgvOptions,} from '../argv'

export interface IProgressStep {
  message: string,
  handler: () => Promise<void>
}

export interface IInstallModuleParams {
  responses: object,
  projectName: string,
}

export interface IModuleCliConfig {
  moduleName: string
  projectDir: string,
  moduleDir: string,
  dependencies?: string[],
  linkDependencies?: string[],
  globalDependencies?: string[],
  createDir?: string[],
  config?: object,
  copy?: [string, string][],
}

export interface IModuleResume {
  title: string,
  description: string,
  value: string,
}

export interface IModuleCli extends IModuleResume {
  create: () => Promise<object>,
  prepareInstall: (params: IInstallModuleParams) => Promise<IModuleCliConfig>,
}

export interface IModule extends IModuleResume {
  start: () => Promise<void>,
}

export interface IBasicCreateProjectResponse {
  projectName: string,
  modules: string[],
}