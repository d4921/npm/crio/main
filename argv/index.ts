import argv from 'argv'

argv.clear().option([
  {
    name: 'dev',
    short: 'd',
    type: 'boolean',
    description: 'Sets run to development mode (Crio developers only)',
    example: "'crio -d",
  },
])

export const args = argv.run()
export const options = args.options as IArgvOptions
export const targets = args.targets

export interface IArgvOptions {
  dev?: boolean
}