export const calcLimitOffset = (page?: number, perpage?: number): IPaginationLimitOffset => {
  page = page || 0
  perpage = perpage || 10

  const limit = perpage
  const offset = (page * perpage) - perpage

  return { limit, offset: offset >= 0 ? offset : 0, }
}

export interface IPaginationLimitOffset {
  limit: number,
  offset: number,
} 